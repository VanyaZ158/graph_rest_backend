<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;port=5432;dbname=graph_dev_db',
    'username' => 'dev',
    'password' => 'testdev',
    'charset' => 'utf8',
];
