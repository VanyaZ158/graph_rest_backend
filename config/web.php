<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$url = require(__DIR__ . '/url_manager.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute' => 'main/index',
    'components' => [
        'request' => [
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if (empty($response->data)) {
                    return $response;
                }
                
                $responseData = [
                    'success' => $response->isOk,
                ];
                
                if ($response->isOk) {
                    if (array_key_exists('data', $response->data)) {
                        $responseData['data'] = $response->data['data'];
                        $responseData['meta'] = $response->data['meta'];
                    } else {
                        $responseData['data'] = $response->data;
                    }
                    
                    $response->data = $responseData;
                } else {
                    $response->data = [
                        'success' => $response->isSuccessful,
                        'data' => ['errors' => $response->data],
                    ];
                }
            }
        ],
        
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'main/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        
        'urlManager' => $url,
    
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
