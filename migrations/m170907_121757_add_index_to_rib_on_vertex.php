<?php

use yii\db\Migration;

class m170907_121757_add_index_to_rib_on_vertex extends Migration
{
    public function safeUp()
    {
        $this->createIndex(
            'idx-ribs-start_vertex_id-end_vertex_id',
            'ribs',
            ['start_vertex_id', 'end_vertex_id'],
            $unique = true
        );
    }

    public function safeDown()
    {
        echo "m170907_121757_add_index_to_rib_on_vertex cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170907_121757_add_index_to_rib_on_vertex cannot be reverted.\n";

        return false;
    }
    */
}
