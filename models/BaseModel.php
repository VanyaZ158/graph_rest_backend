<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;


class BaseModel extends ActiveRecord
{
    public static function findOrNotFound($id)
    {
        $tempModel = self::findOne($id);
        
        if (empty($tempModel)) {
            throw new NotFoundHttpException();
        }
        return $tempModel;
    }
}