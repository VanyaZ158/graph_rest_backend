<?php

namespace app\models;

use yii\web\NotFoundHttpException;

class Vertex extends BaseModel
{
    public const SCENARIO_CREATE = 'create';
    
    public static function belongsToGraph($graph)
    {
        return self::find()->where(['graph_id' => $graph->id]);
    }
    
    public static function tableName()
    {
        return '{{vertices}}';
    }
    
    public static function findByNameOrNotFound($name)
    {
        $tmpModel = self::findOne(['name' => mb_strtolower($name)]);
        if (empty($tmpModel)) {
            throw new NotFoundHttpException();
        }
        
        return $tmpModel;
    }
    
    public function rules()
    {
        return [
            [['name', 'x', 'y'], 'required'],
            [['x', 'y'], 'integer'],
            [['name'], 'string', 'max' => 10],
            [['name'], 'unique'],
        ];
    }
    
    public function beforeValidate()
    {
        $this->name = mb_strtolower($this->name);
        
        return parent::beforeValidate();
    }
    
    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['name', 'x', 'y'],
        ];
    }
    
    public function getGraph()
    {
        return $this->hasOne(Graph::className(), ['id' => 'graph_id']);
    }
    
    public function getStartRibs()
    {
        return $this->hasMany(Rib::className(), ['start_vertex_id' => 'id']);
    }
    
    public function getFurtherRibs()
    {
        return $this->hasMany(Rib::className(), ['end_vertex_id' => 'id']);
    }
    
    public function setSecuredParams($postData)
    {
        $this->scenario = self::SCENARIO_CREATE;
        $this->attributes = $postData;
    }
}
