<?php

namespace app\components;

use app\models\Vertex;
use PHPAlgorithms\Dijkstra;
use PHPAlgorithms\Dijkstra\Creator;
use Yii;
use yii\base\Object;

class GraphTool extends Object
{
    private $_graph;
    private $_redirects = [];
    private $_redirector = 0;
    private $_baseArray = [];
    
    public function reformatBaseArray($baseArray)
    {
        $this->_baseArray = $baseArray;
        $this->_graph = new Dijkstra(function (Creator $creator) {
            foreach ($this->_baseArray as $value) {
                $startVertex = $value['start_vertex_id'];
                $endVertex = $value['end_vertex_id'];
                $weight = $value['weight'];
                
                $startVertex = $this->makeRedirect($startVertex);
                $endVertex = $this->makeRedirect($endVertex);
                
                $startPoint = $creator->getPoint($startVertex);
                $endPoint = $creator->getPoint($endVertex);
                
                if ($startPoint === null) {
                    $startPoint = $creator->addPoint();
                }
                if ($endPoint === null) {
                    $endPoint = $creator->addPoint();
                }
                
                
                $startPoint->addRelation($endPoint, $weight);
            }
        });
        
    }
    
    private function makeRedirect($el)
    {
        if (array_key_exists($el, $this->_redirects)) {
            return $this->_redirects[$el];
        }
        
        $this->_redirects[$el] = $this->_redirector;
        $this->_redirector++;
        
        return $this->_redirects[$el];
    }
    
    public function findShortestPath($fromVertex, $toVertex)
    {
        $response = Yii::$app->response;
        $fromPoint = $this->_redirects[$fromVertex];
        $toPoint = $this->_redirects[$toVertex];
        $nodes = [];
        
        $paths = $this->_graph->generate($fromPoint);
        
        if (!array_key_exists($toPoint, $paths)) {
            $response->statusCode = 400;
            
            return [
                'error' => 'No Way',
            ];
        }
        
        $path = $paths[$toPoint];
        foreach ($path->nodes as $node) {
            $nodes[] = Vertex::findOne(array_search($node, $this->_redirects))->name;
        }
        
        return [
            'nodes' => $nodes,
            'distance' => $path->distance,
        ];
    }
}
